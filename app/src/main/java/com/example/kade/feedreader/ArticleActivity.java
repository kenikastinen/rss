package com.example.kade.feedreader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
/**
 * Created by Kade on 3.10.2015.
 */
public class ArticleActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        setContentView(R.layout.activity_webview);
        WebView myWebView = (WebView) findViewById(R.id.webView);
        myWebView.loadUrl(url);
    }
}
