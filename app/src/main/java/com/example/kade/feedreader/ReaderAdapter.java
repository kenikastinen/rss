package com.example.kade.feedreader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kade on 3.10.2015.
 */
public class ReaderAdapter extends ArrayAdapter<Entry> {
    private ArrayList<Entry> entrys;

    public ReaderAdapter(Context context, int resource, ArrayList<Entry> entrys) {
        super(context, resource, entrys);
        this.entrys = entrys;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Entry entry = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listitem, parent, false);
        }
        // Lookup view for data population
        ImageView image = (ImageView) convertView.findViewById(R.id.ivImage);
        image.setAdjustViewBounds(true);
        TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
        // Populate the data into the template view using the data object
        image.setImageDrawable(entry.getImage());
        tvTitle.setText(entry.getTitle());
        tvDescription.setText(entry.getDescription());
        // Return the completed view to render on screen
        return convertView;
    }
    }
