package com.example.kade.feedreader;

import android.graphics.drawable.Drawable;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Kade on 28.9.2015.
 */
public class Entry  implements Runnable{
    private String title;
    private String link;
    private String description;
    private String category;
    private String pubdate;
    private String imageUrl;
    private Drawable pic;




    public void setTitle(String x){ this.title = x; }
    public void setDescription(String x){ this.description = x; }
    public void setLink(String x){ this.link = x; }
    public void setCategory(String x){ this.category= x; }
    public void setPubdate(String x){ this.pubdate= x; }
    public void setImageUrl(String x){ this. imageUrl= x; }

    public String getTitle(){ return this.title; }
    public String getDescription(){ return this.description; }
    public String getLink(){ return this.link; }
    public String getCategory(){ return this.category; }
    public String getPubdate(){ return this.pubdate; }
    public String getImageUrl(){ return this.imageUrl; }
    public Drawable getImage(){ return this.pic; }




    @Override
    public void run() {
        URL myUrl = null;
        InputStream inputStream = null;
        try {
            myUrl = new URL(this.imageUrl);
            inputStream = (InputStream)myUrl.getContent();
        } catch (MalformedURLException e) { e.printStackTrace();
        } catch (IOException e) { e.printStackTrace();
        }
        this.pic = Drawable.createFromStream(inputStream, null);
    }
}

