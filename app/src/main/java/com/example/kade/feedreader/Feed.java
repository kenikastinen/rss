package com.example.kade.feedreader;

import java.util.ArrayList;

/**
 * Created by Kade on 22.9.2015.
 */
public class Feed {
    ArrayList<Entry> entrys;
    private String title;
    private String link;
    private String description;
    private String category;

    public Feed(){
        entrys = new ArrayList<>();

    }
    public void addEntry(Entry entry){
        entrys.add(entry);

    }
    public String returnTitles(){
        String ret = "";
        for (Entry e: entrys){
            ret += e.getTitle() + "\n";
        }
        return ret;
    }
    public void setTitle(String x){ this.title = x; }
    public void setDescription(String x){ this.description = x; }
    public void setLink(String x){ this.link = x; }
    public void setCategory(String x){ this.category= x; }
    public String getTitle(){ return this.title; }
    public String getDescription(){ return this.description; }
    public String getLink(){ return this.link; }
    public String getCategory(){ return this.category; }
    public ArrayList<Entry> getEntry(){ return this.entrys; }


}


