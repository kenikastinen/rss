package com.example.kade.feedreader;

import java.util.ArrayList;

/**
 * Created by Kade on 3.10.2015.
 */
public class Repository {
    private static final Repository INSTANCE = new Repository();
    ArrayList<Feed> feeds = new ArrayList<>();


    public static Repository getInstance() {
        return INSTANCE;
    }

    public void addFeed(Feed feed){
        feeds.add(feed);
    }
    public ArrayList<Entry>  getFeed(int i){
        return this.feeds.get(i).getEntry();
    }
}
