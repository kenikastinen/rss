package com.example.kade.feedreader;

import android.util.Log;
import android.widget.Switch;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Kade on 22.9.2015.
 */
public class FeedReader implements Runnable {
    private String print = "";
    private URL url = null;
    private BufferedReader in = null;
    private XmlPullParser parser;
    private Feed feed;
    private Entry entry;
    private String text;
    private String urltext;

    public FeedReader(String urli){
        this.urltext = urli;
    }

    public String getIt(){
        String ret = feed.returnTitles();
        return ret;
    }

    @Override
    public void run() {
        try {
            url = new URL(this.urltext);
            //url = new URL("http://uutimet.net/rss/?show=helsinginsanomat,iltalehti,itviikko,kaleva,mikrobitti,mikropc,nyt,pelitfi,tietokone,yle");

            URLConnection urlConnection = url.openConnection();
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newPullParser();
            parser.setInput(in);
            System.out.println(in);



            boolean insideItem = false;
            int event = parser.getEventType();
            while(event != XmlPullParser.END_DOCUMENT) {
                String tag = parser.getName();

                switch (event){
                    case XmlPullParser.START_TAG:
                        if (tag.equalsIgnoreCase("channel")) {
                            this.feed = new Feed();

                        }
                        if (tag.equalsIgnoreCase("item")) {
                            this.entry = new Entry();
                            insideItem = true;
                        }
                        if (tag.equalsIgnoreCase("enclosure") && insideItem){
                           this.entry.setImageUrl(parser.getAttributeValue(null, "url"));
                        }
                        break;
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (tag.equalsIgnoreCase("channel")) {
                            Repository.getInstance().addFeed(this.feed);
                        }else if (tag.equalsIgnoreCase("item")){
                            Thread t = new Thread(entry);
                            t.start();
                            this.feed.addEntry(this.entry);
                            // else add info
                        }else if (tag.equalsIgnoreCase("title")) {
                            if (insideItem) {
                                this.entry.setTitle(text);
                            } else {
                                this.feed.setTitle(text);
                            }
                        }else if (tag.equalsIgnoreCase("link")) {
                            if (insideItem) {
                                this.entry.setLink(text);
                            } else {
                                this.feed.setLink(text);
                            }
                        } else if (tag.equalsIgnoreCase("description")) {
                            if (insideItem) {
                                this.entry.setDescription(text);
                            } else {
                                this.feed.setDescription(text);
                            }
                        } else if (tag.equalsIgnoreCase("category")) {
                            if (insideItem) {
                                this.entry.setCategory(text);
                            } else {
                                this.feed.setCategory(text);
                            }
                        } else if (tag.equalsIgnoreCase("pubdate")) {
                            this.entry.setPubdate(text);
                        }
                        break;
                    default:
                        break;


                }
                event = parser.next();
            }
        } catch (MalformedURLException e) { e.printStackTrace();
        } catch (IOException e) { e.printStackTrace();
        } catch (XmlPullParserException e) { e.printStackTrace();
        }
   }
}
