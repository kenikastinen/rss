package com.example.kade.feedreader;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends AppCompatActivity  {

    private FeedReader fr;
    LinearLayout ll;
    TextView tw ;
    ListView listView;
    private final String FILENAME = "urls.txt";
    String fileInput = "";

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AssetManager assetManager = getAssets();
        try {
            InputStream files = assetManager.open("urls.txt");
            System.out.println(files);

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        fr = new FeedReader("http://yle.fi/uutiset/rss/paauutiset.rss");
                Thread t = new Thread(fr);
                t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        listView = (ListView) findViewById(R.id.list);
        ReaderAdapter rAdapter = new ReaderAdapter(this, R.layout.listitem, Repository.getInstance().getFeed(0));
        listView.setAdapter(rAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Entry e = (Entry) listView.getItemAtPosition(position);
                String url = e.getLink();
                Intent intent = new Intent(MainActivity.this, ArticleActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            }
        });


    }

    /*public void updateUI(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tw.append(fr.getIt());

            }
        });
    }*/

    }
